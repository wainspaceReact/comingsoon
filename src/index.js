import React from 'react';
import ReactDOM from 'react-dom';
import ComingSoon from './components/ComingSoon';

ReactDOM.render(
  <div>
    <ComingSoon />
  </div>,
  document.getElementById('root')
);

