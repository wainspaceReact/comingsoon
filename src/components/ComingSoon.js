import React from 'react';
import Background from '../images/coming-soon1.jpg';
import './index.css';

var sectionStyle = {
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    maxWidth: "100%",
    maxHeight: "100%",
    margin: "auto",
    overflow: "auto",
    height: "100%",
    width: "100%",
};

const ComingSoon = () => (
 <div  >
   <img src={Background} className="bg" />
 </div>
);

export default ComingSoon;
